<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html> 
<html>
<head>
	<title>Notice List</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/resources/css/jquery.mobile-1.4.5.min.css" />
	<link rel="stylesheet" href="/resources/css/jquery.mobile.icons-1.4.5.min.css" />
	<link rel="stylesheet" href="/resources/css/jquery.mobile.theme-1.4.5.min.css" />
</head>
<body>
<input type="hidden" name="page" id="noticePage" value="2">
<div data-role="page" id="divPage">
	<div data-role="header">
		<h1>공지사항</h1>
	</div>
	<div role="main" class="ui-content">
		<ul data-role="listview" id="ulNotice">
			<c:forEach items="#{noticeList}" var="notice">
			<li data-id="${notice.noticeId}" class="liNotice" style="cursor: pointer;">${notice.title}
			<c:if test="${notice.newYn eq 'Y'}"><font color="red" style="text-decoration: underline;"><strong>NEW</strong></font></c:if>
			</li>
			</c:forEach>
		</ul>
	</div>
	<c:if test="${noticeCount > pageSize}">	
	<div role="button" style="text-align: right;">
		<button class="ui-btn ui-btn-inline ui-mini ui-btn-icon-left ui-icon-arrow-r" id="btnNoticeMore">더 보기</button>
	</div>
	</c:if>
	<div data-role="header">
		<h1>이벤트</h1>
	</div>	
	<div role="main2" class="ui-content">
		<table data-role="table" class="ui-responsive table-stroke">
		<thead>
		<tr><th></th></tr>
		</thead>
      	<tbody>
      	<c:forEach items="#{eventList}" var="event">
      	<tr><td data-id="${event.noticeId}" class="tdEvent" style="cursor: pointer;"><img alt="${event.title}" src="${event.eventBannerUrl}" style="width:100%;"></td></tr>
      	</c:forEach>
      	</tbody>		
		</table>
	</div>
	<c:if test="${eventCount > pageSize}">	
	<div role="button" style="text-align: right;">
		<button class="ui-btn ui-btn-inline ui-mini ui-btn-icon-left ui-icon-arrow-r">더 보기</button>
	</div>
	</c:if>		
	<div data-role="footer">
		<button class="ui-btn-right ui-btn ui-btn-inline ui-mini ui-corner-all ui-btn-icon-left ui-icon-back" id="btnUp">맨위로</button>
	</div>
</div>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
<script type="text/javascript">
$("#btnUp").bind("click",function(){
	$("html, body").animate({scrollTop:0},'fast');
});

$("body").on("click",".liNotice",function(){
	var id = $(this).data('id');
	var page = $("#noticePage").val();
	
	location.href = "/v1/notice/notice_detail.vlt?notice_id=" + id + "&page=" + page + "&pageSize=${pageSize}";
});

/**
 * Stringbuilder
 */
var tools = {
    getStringBuilder: function() {
		var data = [];
		var counter = 0;
		
		return {
			append: function(s) { data[counter++] = s; return this; },
			remove: function(i, j) { data.splice(i, j || 1); return this; },
			insert: function(i, s) { data.splice(i, 0, s); return this; },
			toString: function(s) { return data.join(s || ""); return this; },
		};
	}
};

/**
 * 공지사항 더보기 
 */
$("#btnNoticeMore").bind("click",function(){
	var noticePage = $("#noticePage").val();
	$("#noticePage").val(parseInt(noticePage) + 1);
	
	var param = {game_id:'${gameId}',market:"${market}",lang:"${language}",page:noticePage,category:'common',pageSize:'${pageSize}'};
	
	
	
	$.ajax({
		type: "GET",
		url: "notice_list.json",
		data: param,
		success: function(data) {
			var noticeList = data.noticeList;
			var length = noticeList.length;
			var liNotice = "";
			var sb = tools.getStringBuilder();
			
			for(var index = 0; index < length ; index++){
				sb.append("<li data-id='");
				sb.append(noticeList[index].noticeId);
				sb.append("' class='liNotice' style='cursor: pointer;'>");
				sb.append(noticeList[index].title);
				
				if(noticeList[index].newYn == 'Y'){
					sb.append("<font color='red' style='text-decoration: underline;'><strong>NEW</strong></font>");
				}
				sb.append("</li>");
			}
			
			$('#ulNotice').append(sb.toString()).listview("refresh");    
		}
	});
	
});


$(".tdEvent").bind("click",function(){
	var id = $(this).data('id');
	var page = $("#page").val();
	
	location.href = "/v1/notice/notice_detail.vlt?notice_id=" + id + "&page=" + page + "&pageSize=${pageSize}";
});

</script>
</body>
</html>