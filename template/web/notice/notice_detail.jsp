<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html> 
<html>
<head>
	<title>Notice Detail</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile.icons-1.4.5.min.css" />
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.theme-1.4.5.min.css" />
</head>
<body>
<div data-role="page" id="divPage">
	<div data-role="header">
		<h1>${detail.title}</h1>
	</div>
	<div role="main" class="ui-content">
	<div class="data-role-none">
	${detail.content}
	</div>	
	</div>
	<div data-role="footer">
		<button class="ui-btn-left ui-btn ui-btn-inline ui-mini ui-corner-all ui-btn-icon-left ui-icon-grid" id="btnList">목록으로</button>
		<button class="ui-btn-right ui-btn ui-btn-inline ui-mini ui-corner-all ui-btn-icon-left ui-icon-back" id="btnUp">맨위로</button>
	</div>
</div>
<script src="/resources/js/reset.jqm.js"></script>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
<script type="text/javascript">

$("#btnList").bind("click",function(){
	location.href = "/v1/notice/notice_list.vlt?game_id=${detail.gameId}&market=${detail.market}&lang=${detail.language}&pageSize=${pageSize}";
});

$("#btnUp").bind("click",function(){
	$("html, body").animate({scrollTop:0},'fast');
});
</script>
</body>
</html>