/**
 * blesu
 * 2016. 3. 15. 오전 10:24:50
 */
package com.ej.platform.model.game;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import lombok.Data;

/**
 * @author blesu
 *
 */
@Data
@Alias("gameMember")
public class GameMember implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -313690625885719861L;

	private int gameMemberId;
	private int memberId;
	private int gameId;
	private String token;
	private String pushYn;
	private String pushCode;
	private String mobileModel;
	private String osVersion;
	private String mobileId;
	private String countryCode;
	private String langCode;
	private String matket;
	private String updateDate;
	private Date createDate;
	//
	private String gameDB;

	/*
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "GameMember [gameMemberId=" + gameMemberId + ", memberId=" + memberId + ", gameId=" + gameId + ", token="
				+ token + ", pushYn=" + pushYn + ", pushCode=" + pushCode + ", mobileModel=" + mobileModel
				+ ", osVersion=" + osVersion + ", mobileId=" + mobileId + ", countryCode=" + countryCode + ", langCode="
				+ langCode + ", matket=" + matket + ", updateDate=" + updateDate + ", createDate=" + createDate
				+ ", gameDB=" + gameDB + "]";
	}
}
