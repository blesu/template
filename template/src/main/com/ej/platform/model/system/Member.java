/**
 * blesu
 * 2016. 3. 10. 오후 2:40:04
 */
package com.ej.platform.model.system;

import org.apache.ibatis.type.Alias;

import lombok.Data;

/**
 * @author blesu
 *
 */
@Data
@Alias("member")
public class Member {
	private String id;
	private String provider;
	private int memberId;
	private String email;
	private String name;
	private String sex;
	private String pictureUrl;
	private String lastLoginDate;
	private String updateDate;
	private String createDate;
}
