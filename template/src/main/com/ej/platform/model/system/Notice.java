/**
 * blesu
 * 2016. 3. 30. 오후 6:10:25
 */
package com.ej.platform.model.system;

import java.util.Date;

import org.apache.ibatis.type.Alias;

import lombok.Data;

/**
 * @author blesu
 *
 */
@Data
@Alias("notice")
public class Notice {
	private int noticeId;
	private String title;
	private String eventBannerUrl;
	private String content;
	private String category;
	private int gameId;
	private String market;
	private String language;
	private int order;
	private int readCount;
	private String newYn;
	private Date createDate;
}
