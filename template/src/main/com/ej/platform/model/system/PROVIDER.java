/**
 * blesu
 * 2016. 3. 17. 오전 10:42:46
 */
package com.ej.platform.model.system;

import java.util.EnumSet;

import org.apache.commons.lang.StringUtils;

/**
 * @author blesu
 *
 */
public enum PROVIDER {
	FACEBOOK("facebook"), GPLUS("gplus"), GUEST("guest");

	private String value;

	PROVIDER(String typeValue) {
		this.value = typeValue;
	}

	public String getValue() {
		return value;
	}

	private static PROVIDER getDefaultEnumOrderStat() {
		return GUEST;
	}

	public static PROVIDER getEnumOrderStat(String orderStat) {
		PROVIDER retrunType = getDefaultEnumOrderStat();

		for (final PROVIDER element : EnumSet.allOf(PROVIDER.class)) {
			if (StringUtils.equals(element.toString(), orderStat)) {
				retrunType = element;
				break;
			}
		}
		return retrunType;
	}

	@Override
	public String toString() {
		return value;
	}
}
