/**
 * blesu
 * MemberMapper.java
 * 2016. 2. 23. �삤�쟾 11:58:00
 */
package com.ej.platform.mapper.system;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.ej.platform.model.system.Member;

/**
 * @author blesu
 *
 */
@Repository
public interface SystemMapper {
	/**
	 * 
	 * @param id
	 * @return
	 */
	public int selectSequenceByName(@Param("sequenceName") String sequenceName);

	/**
	 * 회원 추가
	 * 
	 * @param email
	 * @param password
	 * @param provideCode
	 * @param name
	 * @param sex
	 * @param pictureUrl
	 * @return
	 */
	public void createMember(@Param("uid") int uid, @Param("id") String id, @Param("provider") String provider,
			@Param("password") String password, @Param("email") String email, @Param("name") String name,
			@Param("sex") String sex, @Param("pictureUrl") String pictureUrl, @Param("mobileId") String mobileId);

	/**
	 * 회원 조회
	 * 
	 * @param id
	 * @param provider
	 * @return
	 */
	public Member selectMember(@Param("id") String id, @Param("provider") String provider);

	/**
	 * 
	 * @param mobileId
	 * @return
	 */
	public Member selectMemberByProviderAndMobileId(@Param("provider") String provider,
			@Param("mobileId") String mobileId);

	/**
	 * 게임 DB 코드목록 조회
	 * 
	 * @param gameId
	 * @return
	 */
	public List<String> selectGameDBListByGameId(@Param("gameId") int gameId);

	/**
	 * 게임 DB 코드 조회
	 * 
	 * @param gameId
	 * @param weight
	 * @return
	 */
	public String selectGameDBCodeByGameIdAndWeight(@Param("gameId") int gameId, @Param("weight") int weight);
}
