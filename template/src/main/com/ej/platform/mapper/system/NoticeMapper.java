/**
 * blesu
 * 2016. 3. 29. 오후 8:33:22
 */
package com.ej.platform.mapper.system;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.ej.platform.model.system.Notice;

/**
 * @author blesu
 *
 */
@Repository
public interface NoticeMapper {
	/**
	 * 공지 목록 수 : 분류
	 * 
	 * @param gameId
	 * @param market
	 * @param language
	 * @param category
	 * @return
	 */
	public int selectNoticeCountByCategory(@Param("gameId") int gameId, @Param("market") String market,
			@Param("language") String language, @Param("category") String category, @Param("newDay") int newDay);

	/**
	 * 
	 * @param gameId
	 * @param market
	 * @param language
	 * @param category
	 * @param newDay
	 * @return
	 */
	public int selectNoticeNewMin(@Param("gameId") int gameId, @Param("market") String market,
			@Param("language") String language, @Param("category") String category, @Param("newDay") int newDay);

	/**
	 * 공지 목록 : 분류
	 * 
	 * @param gameId
	 * @param market
	 * @param language
	 * @param category
	 * @param offest
	 * @param pageSize
	 * @return
	 */
	public List<Notice> selectNoticeListByCategory(@Param("gameId") int gameId, @Param("market") String market,
			@Param("language") String language, @Param("category") String category, @Param("newDay") int newDay,
			@Param("offset") int offset, @Param("pageSize") int pageSize);

	/**
	 * 공지 상세
	 * 
	 * @param noticeId
	 * @return
	 */
	public Notice selectNotice(@Param("noticeId") int noticeId, @Param("newDay") int newDay);

	/**
	 * 
	 * @param noticeId
	 */
	public void updateNoticeReadCount(@Param("noticeId") int noticeId);

}
