/**
 * blesu
 * 2016. 3. 25. 오후 12:11:40
 */
package com.ej.platform.mapper.log;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author blesu
 *
 */
@Repository
public interface LogMapper {
	/**
	 * 로그인 / 로그아웃 로그 추가
	 * 
	 * @param memberId
	 * @param provider
	 * @param gameMemberId
	 * @param gameId
	 * @param status
	 * @param ip
	 */
	public void createLoginLog(@Param("memberId") int memberId, @Param("provider") String provider,
			@Param("gameMemberId") int gameMemberId, @Param("gameId") int gameId, @Param("status") String status,
			@Param("ip") String ip);
}
