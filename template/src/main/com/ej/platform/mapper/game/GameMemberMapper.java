/**
 * blesu
 * MemberMapper.java
 * 2016. 2. 23. �삤�쟾 11:58:00
 */
package com.ej.platform.mapper.game;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.ej.platform.model.game.GameMember;

/**
 * @author blesu
 *
 */
@Repository
public interface GameMemberMapper {

	/**
	 * 기기 id로 게임회원 조회
	 * 
	 * @param mobileId
	 * @return
	 */
	public GameMember selectGameMemberByMobileId(@Param("mobileId") String mobileId);

	/**
	 * 
	 * @param token
	 * @return
	 */
	public GameMember selectGameMemberByToken(@Param("token") String token);

	/**
	 * 게임회원 추가
	 * 
	 * @param memberId
	 * @param token
	 * @param pushYn
	 * @param pushCode
	 * @param mobileModel
	 * @param osVersion
	 * @param countryCode
	 * @param langCode
	 * @param market
	 * @return
	 */
	public void createGameMember(@Param("gameMemberId") int gameMemberId, @Param("memberId") int memberId,
			@Param("gameId") int gameId, @Param("token") String token, @Param("pushYn") String pushYn,
			@Param("pushCode") String pushCode, @Param("mobileModel") String mobileModel,
			@Param("osVersion") String osVersion, @Param("mobileId") String mobileId,
			@Param("countryCode") String countryCode, @Param("langCode") String langCode,
			@Param("market") String market, @Param("ip") String ip);

	/**
	 * 게임회원 조회 : 회원 ID, 게임 ID
	 * 
	 * @param memberId
	 * @param gameId
	 * @return
	 */
	public GameMember selectGameMemberByMemberIdAndGameId(@Param("memberId") int memberId, @Param("gameId") int gameId);

	/**
	 * 게임회원 조회
	 * 
	 * @param gameMemberId
	 * @return
	 */
	public GameMember selectGameMember(@Param("gameMemberId") int gameMemberId);

	/**
	 * 게임회원 로그인 정보 업데이트
	 * 
	 * @param memberId
	 * @param gameId
	 * @param token
	 * @param pushCode
	 * @param mobileModel
	 * @param osVersion
	 * @param mobileId
	 * @param countryCode
	 * @param langCode
	 * @param market
	 */
	public void updateGameMemberLogin(@Param("gameMemberId") int gameMemberId, @Param("gameId") int gameId,
			@Param("token") String token, @Param("pushCode") String pushCode, @Param("mobileModel") String mobileModel,
			@Param("osVersion") String osVersion, @Param("mobileId") String mobileId,
			@Param("countryCode") String countryCode, @Param("langCode") String langCode,
			@Param("market") String market);

	/**
	 * 
	 * @param gameMemberId
	 * @param gameId
	 * @param token
	 * @param mobileModel
	 * @param osVersion
	 * @param mobileId
	 * @param countryCode
	 * @param langCode
	 */
	public void updateGameMemberAutoLogin(@Param("gameMemberId") int gameMemberId, @Param("gameId") int gameId,
			@Param("token") String token, @Param("mobileModel") String mobileModel,
			@Param("osVersion") String osVersion, @Param("mobileId") String mobileId,
			@Param("countryCode") String countryCode, @Param("langCode") String langCode);

	/**
	 * 게임유저 토큰 수정
	 * 
	 * @param gameMemberId
	 * @param token
	 */
	public void updateGameMemberToken(@Param("gameMemberId") int gameMemberId, @Param("token") String token);

}
