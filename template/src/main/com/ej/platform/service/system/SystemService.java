/**
 * blesu
 * 2016. 3. 14. 오후 3:10:42
 */
package com.ej.platform.service.system;

/**
 * @author blesu
 *
 */
public interface SystemService {
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getGameDatasourceType(String dsName) throws Exception;

}
