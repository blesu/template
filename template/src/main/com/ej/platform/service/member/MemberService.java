/**
 * blesu
 * 2016. 3. 9. 오전 11:17:28
 */
package com.ej.platform.service.member;

import com.ej.platform.model.game.GameMember;

/**
 * @author blesu
 *
 */
public interface MemberService {
	/**
	 * 시퀀스 가져오기
	 * 
	 * @param seqName
	 * @return
	 * @throws Exception
	 */
	public int getSequence(String seqName) throws Exception;

	/**
	 * 로그인
	 * 
	 * @param id
	 * @param provider
	 * @param email
	 * @param password
	 * @param name
	 * @param picture
	 * @param market
	 * @param pushYn
	 * @param pushCode
	 * @param osVersion
	 * @param mobileModel
	 * @param mobileId
	 * @param country
	 * @param lang
	 * @param ip
	 * @return
	 * @throws Exception
	 */
	public GameMember login(String id, String provider, String password, String email, String name, String sex,
			String pictureUrl, int gameId, String market, String pushYn, String pushCode, String osVersion,
			String mobileModel, String mobileId, String country, String lang, String ip) throws Exception;

	/**
	 * 게스트 로그인
	 * 
	 * @param gameId
	 * @param market
	 * @param pushYn
	 * @param pushCode
	 * @param osVersion
	 * @param mobileModel
	 * @param mobileId
	 * @param country
	 * @param lang
	 * @param ip
	 * @return
	 * @throws Exception
	 */
	public GameMember guestLogin(int gameId, String market, String pushYn, String pushCode, String osVersion,
			String mobileModel, String mobileId, String country, String lang, String ip) throws Exception;

	/**
	 * 간편 로그인
	 * 
	 * @param id
	 * @param provider
	 * @param gameId
	 * @param market
	 * @param pushYn
	 * @param pushCode
	 * @param osVersion
	 * @param mobileModel
	 * @param mobileId
	 * @param country
	 * @param lang
	 * @param ip
	 * @return
	 * @throws Exception
	 */
	public GameMember simpleLogin(String id, String provider, int gameId, String market, String pushYn, String pushCode,
			String osVersion, String mobileModel, String mobileId, String country, String lang, String ip)
			throws Exception;

	/**
	 * 로그인 : 자동
	 * 
	 * @param id
	 * @param provider
	 * @param osVersion
	 * @param mobileModel
	 * @param mobileId
	 * @param country
	 * @param lang
	 * @param ip
	 * @return
	 * @throws Exception
	 */
	public GameMember autoLogin(String id, String provider, int gameId, int gameMemberId, String osVersion,
			String mobileModel, String mobileId, String country, String lang, String ip) throws Exception;

	/**
	 * 
	 * @param gameId
	 * @param memberId
	 * @return
	 * @throws Exception
	 */
	public GameMember gameMemberInfo(int gameId, int memberId) throws Exception;

	/**
	 * 토큰 인증
	 * 
	 * @param token
	 * @return
	 * @throws Exception
	 */
	public GameMember authByToken(String token) throws Exception;

	/**
	 * 
	 * @param gameMemberId
	 * @return
	 * @throws Exception
	 */
	public long getTokenValidMillis(int gameMemberId) throws Exception;

	/**
	 * 로그아웃
	 * 
	 * @param token
	 * @throws Exception
	 */
	public void logout(int gameMemberId, String ip) throws Exception;
}
