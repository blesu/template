/**
 * blesu
 * 2016. 3. 9. 오후 4:18:32
 */
package com.ej.platform.service.member;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ej.platform.config.ContextHolder;
import com.ej.platform.config.DataSourceType;
import com.ej.platform.dao.RedisDAO;
import com.ej.platform.mapper.game.GameMemberMapper;
import com.ej.platform.mapper.log.LogMapper;
import com.ej.platform.mapper.system.SystemMapper;
import com.ej.platform.model.game.GameMember;
import com.ej.platform.model.system.Member;
import com.ej.platform.util.AES128Utils;
import com.google.gson.JsonObject;

/**
 * @author blesu
 *
 */
@Service
public class MemberServiceImpl implements MemberService {
	@Value("${enckey}")
	private String enckey;
	@Value("${token.validDay}")
	private int timeout;
	@Autowired
	private SystemMapper systemMapper;
	@Autowired
	private GameMemberMapper gameMemberMapper;
	@Autowired
	private LogMapper logMapper;
	@Autowired
	private RedisDAO redisDAO;

	/*
	 * {@inheritDoc}
	 */
	@Override
	public GameMember login(String id, String provider, String password, String email, String name, String sex,
			String pictureUrl, int gameId, String market, String pushYn, String pushCode, String osVersion,
			String mobileModel, String mobileId, String country, String lang, String ip) throws Exception {
		GameMember gameMember = null;
		int gameMemberId = 0;

		Member member = systemMapper.selectMember(id, provider);

		if (member != null) {
			// login
			int memberId = member.getMemberId();
			List<String> dbCodeList = systemMapper.selectGameDBListByGameId(gameId);

			for (String dbCode : dbCodeList) {
				ContextHolder.setDataSourceType(dbCode);
				gameMember = gameMemberMapper.selectGameMemberByMemberIdAndGameId(member.getMemberId(), gameId);

				if (gameMember != null) {
					break;
				}
			}

			if (gameMember != null) {
				// login
				gameMemberId = gameMember.getGameMemberId();
				String token = this.generateToken(gameMemberId);
				gameMemberMapper.updateGameMemberLogin(gameMemberId, gameId, token, pushCode, mobileModel, osVersion,
						mobileId, country, lang, market);
			} else {
				// join game member
				gameMemberId = this.joinGameMember(gameId, memberId, market, pushYn, pushCode, osVersion, mobileModel,
						mobileId, country, lang, ip);
			}
		} else {
			// join - member, gameMember
			gameMemberId = this.join(id, provider, password, email, name, sex, pictureUrl, gameId, market, pushYn,
					pushCode, osVersion, mobileModel, mobileId, country, lang, ip);
		}

		String gameDB = ContextHolder.getDataSourceType().name();
		gameMember = gameMemberMapper.selectGameMember(gameMemberId);
		gameMember.setGameDB(gameDB);

		System.out.println("======================== : " + gameMember.toString());

		redisDAO.createValueByKey("" + gameMember.getGameMemberId(), gameMember, timeout, TimeUnit.DAYS);

		return gameMember;
	}

	/*
	 * {@inheritDoc}
	 */
	@Override
	public GameMember guestLogin(int gameId, String market, String pushYn, String pushCode, String osVersion,
			String mobileModel, String mobileId, String country, String lang, String ip) throws Exception {
		String provider = "guest";
		GameMember gameMember = null;
		int gameMemberId = 0;
		Member member = systemMapper.selectMemberByProviderAndMobileId(provider, mobileId);

		if (member != null) {
			int memberId = member.getMemberId();
			List<String> dbCodeList = systemMapper.selectGameDBListByGameId(gameId);

			for (String dbCode : dbCodeList) {
				ContextHolder.setDataSourceType(dbCode);
				gameMember = gameMemberMapper.selectGameMemberByMemberIdAndGameId(member.getMemberId(), gameId);

				if (gameMember != null) {
					break;
				}
			}

			if (gameMember != null) {
				// login
				gameMemberId = gameMember.getGameMemberId();
				String token = this.generateToken(gameMemberId);
				gameMemberMapper.updateGameMemberLogin(gameMemberId, gameId, token, pushCode, mobileModel, osVersion,
						mobileId, country, lang, market);
			} else {
				// join game member
				gameMemberId = this.joinGameMember(gameId, memberId, market, pushYn, pushCode, osVersion, mobileModel,
						mobileId, country, lang, ip);
			}
		} else {
			gameMemberId = this.join("", provider, "ej~!23", "", "", "", "", gameId, market, pushYn, pushCode,
					osVersion, mobileModel, mobileId, country, lang, ip);
		}

		String gameDB = ContextHolder.getDataSourceType().name();
		gameMember = gameMemberMapper.selectGameMember(gameMemberId);
		gameMember.setGameDB(gameDB);

		redisDAO.createValueByKey("" + gameMember.getGameMemberId(), gameMember, timeout, TimeUnit.DAYS);

		return gameMember;
	}

	/**
	 * 게임회원 추가
	 * 
	 * @param gameId
	 * @param memberId
	 * @throws Exception
	 */
	private int joinGameMember(int gameId, int memberId, String market, String pushYn, String pushCode,
			String osVersion, String mobileModel, String mobileId, String country, String lang, String ip)
			throws Exception {
		Random random = new Random();
		int weight = random.nextInt(100) + 1;
		String dbCode = systemMapper.selectGameDBCodeByGameIdAndWeight(gameId, weight);
		int gameMemberId = this.getSequence("gid");
		String token = this.generateToken(gameMemberId);

		ContextHolder.setDataSourceType(dbCode);
		gameMemberMapper.createGameMember(gameMemberId, memberId, gameId, token, pushYn, pushCode, mobileModel,
				osVersion, mobileId, country, lang, market, ip);

		return gameMemberId;
	}

	/**
	 * 전체 회원 / 게임회원 가입
	 * 
	 * @param id
	 * @param provider
	 * @param password
	 * @param email
	 * @param name
	 * @param sex
	 * @param pictureUrl
	 * @param gameId
	 * @param market
	 * @param pushYn
	 * @param pushCode
	 * @param osVersion
	 * @param mobileModel
	 * @param mobileId
	 * @param country
	 * @param lang
	 * @param ip
	 * @param token
	 * @return
	 * @throws Exception
	 */
	private int join(String id, String provider, String password, String email, String name, String sex,
			String pictureUrl, int gameId, String market, String pushYn, String pushCode, String osVersion,
			String mobileModel, String mobileId, String country, String lang, String ip) throws Exception {
		int uid = this.getSequence("uid");
		systemMapper.createMember(uid, id, provider, password, email, name, sex, pictureUrl, mobileId);

		return this.joinGameMember(gameId, uid, market, pushYn, pushCode, osVersion, mobileModel, mobileId, country,
				lang, ip);
	}

	/**
	 * 
	 * @param userId
	 * @param gameDB
	 * @return
	 * @throws Exception
	 */
	private String generateToken(int gameMemberId) throws Exception {
		JsonObject jsonObj = new JsonObject();
		jsonObj.addProperty("uid", gameMemberId);
		jsonObj.addProperty("time", System.currentTimeMillis());

		return AES128Utils.encrypt(enckey, jsonObj.toString());
	}

	/**
	 * 
	 * @param token
	 * @return
	 * @throws Exception
	 */
	private Integer parseToken(String token) throws Exception {
		token = AES128Utils.decrypt(enckey, token);
		JSONParser parser = new JSONParser();
		JSONObject jsonObj = (JSONObject) parser.parse(token);
		Long uid = (Long) jsonObj.get("uid");

		return new Integer(uid.intValue());
	}

	/*
	 * {@inheritDoc}
	 */
	@Override
	public GameMember gameMemberInfo(int gameId, int memberId) throws Exception {
		GameMember gameMember = gameMemberMapper.selectGameMemberByMemberIdAndGameId(memberId, gameId);

		DataSourceType dsType = ContextHolder.getDataSourceType();
		gameMember.setGameDB(dsType.name());

		return gameMemberMapper.selectGameMemberByMemberIdAndGameId(memberId, gameId);
	}

	/*
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(value = "systemTransactionManager", propagation = Propagation.REQUIRES_NEW)
	public int getSequence(String seqName) throws Exception {
		return systemMapper.selectSequenceByName(seqName);
	}

	/*
	 * {@inheritDoc}
	 */
	@Override
	public GameMember authByToken(String token) throws Exception {
		int gameMemberId = this.parseToken(token);
		GameMember gameMember = (GameMember) redisDAO.selectValueByKey("" + gameMemberId);

		return gameMember;
	}

	/*
	 * {@inheritDoc}
	 */
	@Override
	public void logout(int gameMemberId, String ip) throws Exception {
		redisDAO.deleteValueByKey("" + gameMemberId);
		gameMemberMapper.updateGameMemberToken(gameMemberId, "");
		GameMember gameMember = gameMemberMapper.selectGameMember(gameMemberId);

		logMapper.createLoginLog(gameMember.getMemberId(), "", gameMemberId, gameMember.getGameId(), "logout", ip);
	}

	/*
	 * {@inheritDoc}
	 */
	@Override
	public GameMember autoLogin(String id, String provider, int gameId, int gameMemberId, String osVersion,
			String mobileModel, String mobileId, String country, String lang, String ip) throws Exception {
		// TODO : login log, cache time
		GameMember gameMember = null;
		List<String> dbCodeList = systemMapper.selectGameDBListByGameId(gameId);

		for (String dbCode : dbCodeList) {
			ContextHolder.setDataSourceType(dbCode);
			gameMember = gameMemberMapper.selectGameMember(gameMemberId);

			if (gameMember != null) {
				String token = this.generateToken(gameMemberId);
				gameMemberMapper.updateGameMemberAutoLogin(gameMemberId, gameId, token, mobileModel, osVersion,
						mobileId, country, lang);
				break;
			}
		}

		gameMember = gameMemberMapper.selectGameMember(gameMemberId);

		String gameDB = ContextHolder.getDataSourceType().name();
		gameMember.setGameDB(gameDB);
		redisDAO.createValueByKey("" + gameMemberId, gameMember, timeout, TimeUnit.DAYS);

		return gameMember;
	}

	/*
	 * {@inheritDoc}
	 */
	@Override
	public long getTokenValidMillis(int gameMemberId) throws Exception {
		return redisDAO.selectExpireMillis("" + gameMemberId);
	}

	/*
	 * {@inheritDoc}
	 */
	@Override
	public GameMember simpleLogin(String id, String provider, int gameId, String market, String pushYn, String pushCode,
			String osVersion, String mobileModel, String mobileId, String country, String lang, String ip)
			throws Exception {
		GameMember gamemember = null;
		Member member = systemMapper.selectMember(mobileId, provider);
		int memberId = member.getMemberId();

		List<String> dbCodeList = systemMapper.selectGameDBListByGameId(gameId);

		for (String dbCode : dbCodeList) {
			ContextHolder.setDataSourceType(dbCode);
			gamemember = gameMemberMapper.selectGameMemberByMemberIdAndGameId(memberId, gameId);

			if (gamemember != null) {
				break;
			}
		}

		if (gamemember != null) {
			int gamememberId = gamemember.getGameMemberId();
			String token = this.generateToken(gamememberId);
			gameMemberMapper.updateGameMemberLogin(gamememberId, gameId, token, pushCode, mobileModel, osVersion,
					mobileId, country, lang, market);
		} else {
			this.joinGameMember(gameId, memberId, market, pushYn, pushCode, osVersion, mobileModel, mobileId, country,
					lang, ip);
		}

		String gameDB = ContextHolder.getDataSourceType().name();
		gamemember.setGameDB(gameDB);

		redisDAO.createValueByKey("" + gamemember.getGameMemberId(), gamemember, timeout, TimeUnit.DAYS);

		return gamemember;
	}

}
