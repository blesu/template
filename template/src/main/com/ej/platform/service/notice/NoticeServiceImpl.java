/**
 * blesu
 * 2016. 3. 31. 오후 2:56:30
 */
package com.ej.platform.service.notice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ej.platform.mapper.system.NoticeMapper;
import com.ej.platform.model.system.Notice;

/**
 * @author blesu
 *
 */
@Service
public class NoticeServiceImpl implements NoticeService {
	@Value("${notice.newDay}")
	private int newDay;
	@Autowired
	private NoticeMapper noticeMapper;

	/*
	 * {@inheritDoc}
	 */
	@Override
	public int noticeCount(int gameId, String market, String language, String category) throws Exception {
		return noticeMapper.selectNoticeCountByCategory(gameId, market, language, category, newDay);
	}

	/*
	 * {@inheritDoc}
	 */
	@Override
	public List<Notice> noticeList(int gameId, String market, String language, String category, int offset,
			int pageSize) throws Exception {
		return noticeMapper.selectNoticeListByCategory(gameId, market, language, category, gameId, offset, pageSize);
	}

	/*
	 * {@inheritDoc}
	 */
	@Override
	public Notice noticeDetail(int noticeId) throws Exception {
		noticeMapper.updateNoticeReadCount(noticeId);
		return noticeMapper.selectNotice(noticeId, newDay);
	}

	/*
	 * {@inheritDoc}
	 */
	@Override
	public int showMinNoticeId(int gameId, String market, String language, String category) throws Exception {
		return noticeMapper.selectNoticeNewMin(gameId, market, language, category, newDay);
	}

}
