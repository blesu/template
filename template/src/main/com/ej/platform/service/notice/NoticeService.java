/**
 * blesu
 * 2016. 3. 31. 오전 11:46:13
 */
package com.ej.platform.service.notice;

import java.util.List;

import com.ej.platform.model.system.Notice;

/**
 * @author blesu
 *
 */
public interface NoticeService {
	/**
	 * 
	 * @param gameId
	 * @param market
	 * @param language
	 * @param category
	 * @return
	 * @throws Exception
	 */
	public int noticeCount(int gameId, String market, String language, String category) throws Exception;

	/**
	 * 
	 * @param gameId
	 * @param market
	 * @param language
	 * @param category
	 * @return
	 * @throws Exception
	 */
	public int showMinNoticeId(int gameId, String market, String language, String category) throws Exception;

	/**
	 * 
	 * @param gameId
	 * @param market
	 * @param language
	 * @param category
	 * @return
	 * @throws Exception
	 */
	public List<Notice> noticeList(int gameId, String market, String language, String category, int offset,
			int pageSize) throws Exception;

	/**
	 * 
	 * @param noticeId
	 * @return
	 * @throws Exception
	 */
	public Notice noticeDetail(int noticeId) throws Exception;
}
