/**
 * blesu
 * 2016. 3. 10. 오후 6:42:53
 */
package com.ej.platform.config;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @author blesu
 *
 */
public class RoutingDataSource extends AbstractRoutingDataSource {

	/*
	 * {@inheritDoc}
	 */
	@Override
	protected Object determineCurrentLookupKey() {
		return ContextHolder.getDataSourceType();
	}

}
