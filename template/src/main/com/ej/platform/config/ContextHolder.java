/**
 * blesu
 * 2016. 3. 10. 오후 6:43:48
 */
package com.ej.platform.config;

/**
 * @author blesu
 *
 */
public class ContextHolder {
	private static final ThreadLocal<DataSourceType> contextHolder = new ThreadLocal<DataSourceType>();

	public static void setDataSourceType(String dbName) {
		DataSourceType dataSourceType = DataSourceType.valueOf(dbName);
		contextHolder.set(dataSourceType);
	}

	public static void setDataSourceType(DataSourceType dataSourceType) {
		contextHolder.set(dataSourceType);
	}

	public static DataSourceType getDataSourceType() {
		return contextHolder.get();
	}

	public static void clearDataSourceType() {
		contextHolder.remove();
	}
}
