/**
 * blesu
 * 2016. 3. 10. 오후 6:49:13
 */
package com.ej.platform.config;

/**
 * @author blesu
 *
 */
public enum DataSourceType {
	SYSTEM, GAME01, GAME02;
}
