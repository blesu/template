package com.ej.platform.interceptor;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.ej.platform.config.ContextHolder;
import com.ej.platform.model.game.GameMember;
import com.ej.platform.service.member.MemberService;

/**
 * @author blesu
 */
public class CheckAuthInterceptor extends HandlerInterceptorAdapter {
	@Autowired
	private MemberService memberService;

	final Log log = LogFactory.getLog(getClass());

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String path = request.getServletPath();
		Enumeration<String> params = request.getParameterNames();

		log.info("================= [S] parameter =================");
		while (params.hasMoreElements()) {
			String names = params.nextElement();
			log.info(names + " : " + request.getParameter(names) + "\n");
		}
		log.info("================= [E] parameter =================");

		if (this.checkExceptPath(path)) {
			return true;
		} else {
			String token = request.getHeader("token");

			if (StringUtils.isNotBlank(token)) {
				GameMember gameMember = memberService.authByToken(token);

				if (gameMember != null) {

					ContextHolder.setDataSourceType(gameMember.getGameDB());
					request.setAttribute("gid", gameMember.getGameMemberId());

					return true;
				} else {
					response.sendError(401);
					return false;
				}
			} else {
				response.sendError(400);
				return false;
			}
		}
	}

	/**
	 * 
	 * 
	 * @param path
	 * @return
	 */
	private boolean checkExceptPath(String path) {
		boolean status = false;
		String[] exceptPathArr = { "/v1/member/login.json", "/v1/member/simple_login.json",
				"/v1/member/auto_login.json", "/v1/member/guest_login.json" };

		for (String exceptPath : exceptPathArr) {
			if (path.equals(exceptPath)) {
				status = true;
				break;
			}
		}

		return status;
	}

	/*
	 * {@inheritDoc}
	 */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}
}
