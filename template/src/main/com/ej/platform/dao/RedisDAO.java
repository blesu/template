/**
 * blesu
 * 2016. 3. 22. 오후 4:54:13
 */
package com.ej.platform.dao;

import java.util.concurrent.TimeUnit;

/**
 * @author blesu
 *
 */
public interface RedisDAO {

	/**
	 * 
	 * @param key
	 * @return
	 */
	public Object selectValueByKey(String key);

	/**
	 * 
	 * @param key
	 * @param value
	 */
	public void createValueByKey(String key, Object value, long timeout, TimeUnit timeUnit);

	/**
	 * 
	 * @param key
	 */
	public void deleteValueByKey(String key);

	/**
	 * 
	 * @param key
	 * @return
	 */
	public long selectExpireMillis(String key);
}
