/**
 * blesu
 * 2016. 3. 22. 오후 4:59:55
 */
package com.ej.platform.dao;

import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

/**
 * @author blesu
 *
 */
@Repository
public class RedisDAOImpl implements RedisDAO {
	@Resource(name = "redisTemplate")
	protected ValueOperations<String, Object> redisClient;

	@Resource(name = "redisTemplate")
	protected RedisTemplate<String, Object> redisTemplate;

	/*
	 * {@inheritDoc}
	 */
	@Override
	public Object selectValueByKey(String key) {
		return redisClient.get(key);
	}

	/*
	 * {@inheritDoc}
	 */
	@Override
	public void createValueByKey(String key, Object value, long timeout, TimeUnit timeUnit) {
		redisClient.set(key, value, timeout, timeUnit);
	}

	/*
	 * {@inheritDoc}
	 */
	@Override
	public void deleteValueByKey(String key) {
		redisClient.set(key, null);
	}

	/*
	 * {@inheritDoc}
	 */
	@Override
	public long selectExpireMillis(String key) {
		return redisTemplate.getExpire(key, TimeUnit.MILLISECONDS);
	}

}
