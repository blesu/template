/**
 * blesu
 * 2016. 3. 9. 오전 11:16:46
 */
package com.ej.platform.controller.member.v1;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ej.platform.controller.AbstractController;
import com.ej.platform.service.member.MemberService;

/**
 * @author blesu
 *
 */
@RestController
@RequestMapping("/v1/member")
public class LogoutController extends AbstractController {
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private MemberService memberService;

	@RequestMapping(value = "/logout.json", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ModelMap> execute(ModelMap model) throws Exception {
		HttpStatus stat = HttpStatus.OK;
		String ip = request.getLocalAddr();

		int gameMemberId = this.getGameMemberId();
		memberService.logout(gameMemberId, ip);

		model.addAttribute("logout", true);
		return new ResponseEntity<ModelMap>(model, stat);
	}
}