/**
 * blesu
 * 2016. 3. 9. 오전 11:16:46
 */
package com.ej.platform.controller.member.v1;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ej.platform.model.game.GameMember;
import com.ej.platform.service.member.MemberService;

/**
 * @author blesu
 *
 */
@RestController
@RequestMapping("/v1/member")
public class AutoLoginController {
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private MemberService memberService;

	@RequestMapping(value = "/auto_login.json", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ModelMap> execute(@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "provider", required = true) String provider,
			@RequestParam(value = "gameId", required = true) int gameId,
			@RequestParam(value = "userId", required = false) int gameMemberId,
			@RequestParam(value = "osVersion", defaultValue = "") String osVersion,
			@RequestParam(value = "mobileModel", defaultValue = "") String mobileModel,
			@RequestParam(value = "mobileId", defaultValue = "") String mobileId,
			@RequestParam(value = "country", required = false, defaultValue = "kr") String country,
			@RequestParam(value = "lang", required = false, defaultValue = "kr") String lang, ModelMap model)
			throws Exception {
		HttpStatus stat = HttpStatus.OK;
		String ip = request.getLocalAddr();

		GameMember gameMember = memberService.autoLogin(id, provider, gameId, gameMemberId, osVersion, mobileModel,
				mobileId, country, lang, ip);

		if (gameMember != null) {
			model.addAttribute("user_id", gameMember.getGameMemberId());
			model.addAttribute("token", gameMember.getToken());
			model.addAttribute("login_date", gameMember.getUpdateDate());
		} else {
			stat = HttpStatus.UNAUTHORIZED;
		}

		return new ResponseEntity<ModelMap>(model, stat);
	}
}