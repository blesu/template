/**
 * AbstractPagingController.java
 * @date : 2013. 6. 21. �삤�썑 5:57:46
 */
package com.ej.platform.controller;

/**
 * @author ej@naogames.com
 */
public abstract class AbstractPagingController extends AbstractController {
	public int getOffset(int page, int listSize) {
		return (page - 1) * listSize;
	}

	public int getTotalPageNum(int totalCount, int listSize) {
		int totalPage = totalCount / listSize;

		if ((totalCount % listSize) > 0) {
			totalPage = totalPage + 1;
		}

		return totalPage;
	}

	protected Page getStartEndPage(int totalPage, int pageSize, int curPage) {

		Page page = new Page();

		int pageScope = curPage / pageSize;

		if (curPage % pageSize > 0) {
			pageScope++;
		}

		int startPage = ((pageScope - 1) * pageSize) + 1;
		int endPage = ((pageScope) * pageSize);

		if (endPage > totalPage) {
			endPage = totalPage;
		}

		page.setStartPage(startPage);
		page.setEndPage(endPage);

		return page;
	}

	public class Page {
		private int startPage;
		private int endPage;

		public int getStartPage() {
			return startPage;
		}

		public void setStartPage(int startPage) {
			this.startPage = startPage;
		}

		public int getEndPage() {
			return endPage;
		}

		public void setEndPage(int endPage) {
			this.endPage = endPage;
		}

	}
}
