/**
 * blesu
 * 2016. 3. 9. 오전 11:16:46
 */
package com.ej.platform.controller.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ej.platform.controller.AbstractController;
import com.ej.platform.service.member.MemberService;

/**
 * @author blesu
 *
 */
@RestController
@RequestMapping("/member")
public class CheckTokenController extends AbstractController {
	@Autowired
	private MemberService memberService;

	@RequestMapping(value = "/check_token.json", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ModelMap> execute(ModelMap model) throws Exception {
		HttpStatus stat = HttpStatus.OK;
		int gameMemberId = this.getGameMemberId();
		long millis = memberService.getTokenValidMillis(gameMemberId);

		model.addAttribute("user_id", gameMemberId);
		model.addAttribute("expire_millis", millis);
		return new ResponseEntity<ModelMap>(model, stat);
	}
}
