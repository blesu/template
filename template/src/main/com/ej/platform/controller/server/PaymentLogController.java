/**
 * blesu
 * 2016. 3. 9. 오전 11:16:46
 */
package com.ej.platform.controller.server;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author blesu
 *
 */
@RestController
@RequestMapping("/payment")
public class PaymentLogController {

	@RequestMapping(value = "/payment_log.json", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ModelMap> execute(@RequestParam(value = "p_order_id", required = true) String partnerOrderId,
			@RequestParam(value = "os", required = true) String os,
			@RequestParam(value = "platform", required = true) String platform,
			@RequestParam(value = "country", required = true) String country,
			@RequestParam(value = "price", required = true) float price,
			@RequestParam(value = "currency", required = true) String currency,
			@RequestParam(value = "user_id", required = true) int gameMemberId,
			@RequestParam(value = "order_id", required = true) String orderId,
			@RequestParam(value = "purchase_id", required = false) String purchaseId,
			@RequestParam(value = "purchase_token", required = true) String purchaseToken,
			@RequestParam(value = "inapp_purchase_data", required = false) String inappPurchaseData,
			@RequestParam(value = "inapp_data_signature", required = false) String inappDataSignature,
			@RequestParam(value = "product_id", required = true) String productId, ModelMap model) throws Exception {
		HttpStatus stat = HttpStatus.OK;

		return new ResponseEntity<ModelMap>(model, stat);
	}
}