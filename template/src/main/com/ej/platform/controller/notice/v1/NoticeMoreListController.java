/**
 * blesu
 * 2016. 3. 9. 오전 11:16:46
 */
package com.ej.platform.controller.notice.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ej.platform.controller.AbstractPagingController;
import com.ej.platform.model.system.Notice;
import com.ej.platform.service.notice.NoticeService;

/**
 * @author blesu
 *
 */
@Controller
@RequestMapping("/v1/notice")
public class NoticeMoreListController extends AbstractPagingController {
	@Autowired
	private NoticeService noticeService;

	@RequestMapping(value = "/notice_list.json", method = RequestMethod.GET)
	public ResponseEntity<ModelMap> execute(@RequestParam(value = "game_id", required = true) int gameId,
			@RequestParam(value = "market", required = true) String market,
			@RequestParam(value = "lang", required = false, defaultValue = "kr") String language,
			@RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "category", required = true) String category,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize, ModelMap model) throws Exception {
		HttpStatus stat = HttpStatus.OK;

		int offset = this.getOffset(page, pageSize);
		List<Notice> noticeList = noticeService.noticeList(gameId, market, language, category, offset, pageSize);

		model.addAttribute("noticeList", noticeList);
		return new ResponseEntity<ModelMap>(model, stat);
	}
}