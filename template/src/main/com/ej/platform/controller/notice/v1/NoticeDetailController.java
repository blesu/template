/**
 * blesu
 * 2016. 3. 9. 오전 11:16:46
 */
package com.ej.platform.controller.notice.v1;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ej.platform.controller.AbstractPagingController;
import com.ej.platform.model.system.Notice;
import com.ej.platform.service.notice.NoticeService;

/**
 * @author blesu
 *
 */
@Controller
@RequestMapping("/v1/notice")
public class NoticeDetailController extends AbstractPagingController {
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private HttpServletResponse response;

	@Autowired
	private NoticeService noticeService;

	@RequestMapping(value = "/notice_detail.vlt", method = RequestMethod.GET)
	public String execute(@RequestParam(value = "notice_id", required = true) int noticeId,
			@RequestParam(value = "page", required = false, defaultValue = "kr") int page,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize, ModelMap model) throws Exception {

		Notice detail = noticeService.noticeDetail(noticeId);
		this.readNotice(noticeId, detail.getGameId(), detail.getCategory());
		pageSize = page * pageSize;

		model.addAttribute("detail", detail);
		model.addAttribute("pageSize", pageSize);
		return "/notice/notice_detail";
	}

	/**
	 * 
	 * @param noticeId
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void readNotice(int noticeId, int gameId, String category) throws Exception {
		String cookieValue = null;
		Cookie[] cookieArr = request.getCookies();
		String cookieName = "volt_" + gameId + "_" + category;
		String jsonName = "read";
		JSONObject jsonObject;
		List<Integer> noticeIdList = new ArrayList<Integer>();

		if (cookieArr != null && cookieArr.length > 0) {
			for (Cookie cookie : cookieArr) {
				if (cookieName.equals(cookie.getName())) {
					cookieValue = cookie.getValue();
				}
			}

			if (StringUtils.isNotBlank(cookieValue)) {
				JSONParser parser = new JSONParser();
				jsonObject = (JSONObject) parser.parse(cookieValue);

				List<Long> tempList = (List<Long>) jsonObject.get(jsonName);

				for (Long temp : tempList) {
					noticeIdList.add(temp.intValue());
				}
			}
		}

		boolean alreadyRead = false;

		for (int orgId : noticeIdList) {
			if (orgId == noticeId) {
				alreadyRead = true;
			}
		}

		if (!alreadyRead) {
			noticeIdList.add(noticeId);
			// write cookie
			jsonObject = new JSONObject();
			jsonObject.put(jsonName, noticeIdList);

			Cookie cookie = new Cookie(cookieName, jsonObject.toJSONString());
			response.addCookie(cookie);
		} else {
			// nothing to do
		}
	}
}