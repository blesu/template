/**
 * blesu
 * 2016. 3. 9. 오전 11:16:46
 */
package com.ej.platform.controller.notice.v1;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ej.platform.controller.AbstractPagingController;
import com.ej.platform.model.system.Notice;
import com.ej.platform.service.notice.NoticeService;

/**
 * @author blesu
 *
 */
@Controller
@RequestMapping("/v1/notice")
public class NoticeListController extends AbstractPagingController {
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private HttpServletResponse response;
	@Autowired
	private NoticeService noticeService;

	@RequestMapping(value = "/notice_list.vlt", method = RequestMethod.GET)
	public String execute(@RequestParam(value = "game_id", required = true) int gameId,
			@RequestParam(value = "market", required = true) String market,
			@RequestParam(value = "lang", required = false, defaultValue = "kr") String language,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize, ModelMap model) throws Exception {
		int offset = 0;
		String category = "common";

		int noticeCount = noticeService.noticeCount(gameId, market, language, category);
		List<Notice> noticeList = noticeService.noticeList(gameId, market, language, category, offset, pageSize);
		this.generateNewYn(noticeList, gameId, market, language, category);

		category = "event";
		int eventCount = noticeService.noticeCount(gameId, market, language, category);
		List<Notice> eventList = noticeService.noticeList(gameId, market, language, category, offset, pageSize);
		this.generateNewYn(eventList, gameId, market, language, category);

		model.addAttribute("gameId", gameId);
		model.addAttribute("market", market);
		model.addAttribute("lang", language);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("noticeCount", noticeCount);
		model.addAttribute("noticeList", noticeList);
		model.addAttribute("eventCount", eventCount);
		model.addAttribute("eventList", eventList);
		model.addAttribute("pageSize", pageSize);
		return "/notice/notice_list";
	}

	/**
	 * New yn 처리
	 * 
	 * @param noticeList
	 * @param gameId
	 * @param market
	 * @param language
	 * @param category
	 * @throws Exception
	 */
	private void generateNewYn(List<Notice> noticeList, int gameId, String market, String language, String category)
			throws Exception {
		List<Integer> readNoticeIdList = this.syncReadNoticeIds(gameId, market, language, category);

		for (Integer readNoticeId : readNoticeIdList) {
			for (Notice notice : noticeList) {
				if (readNoticeId == notice.getNoticeId()) {
					notice.setNewYn("N");
				}
			}
		}
	}

	/**
	 * 쿠키에서 읽음 notice id를 동기화 후 가져옴
	 * 
	 * @param gameId
	 * @param category
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private List<Integer> syncReadNoticeIds(int gameId, String market, String language, String category)
			throws Exception {
		int minNoticeId = noticeService.showMinNoticeId(gameId, market, language, category);

		String cookieValue = null;
		Cookie[] cookieArr = request.getCookies();
		String cookieName = "volt_" + gameId + "_" + category;
		String jsonName = "read";
		JSONObject jsonObject;
		List<Integer> noticeIdList = new ArrayList<Integer>();

		if (cookieArr != null && cookieArr.length > 0) {

			for (Cookie cookie : cookieArr) {
				if (cookieName.equals(cookie.getName())) {
					cookieValue = cookie.getValue();
				}
			}

			if (StringUtils.isNotBlank(cookieValue)) {
				JSONParser parser = new JSONParser();
				jsonObject = (JSONObject) parser.parse(cookieValue);
				List<Long> tempList = (List<Long>) jsonObject.get(jsonName);

				for (Long temp : tempList) {
					noticeIdList.add(temp.intValue());
				}
			}
		}

		if (noticeIdList.size() > 0 && minNoticeId > 0) {
			jsonObject = new JSONObject();
			jsonObject.put(jsonName, noticeIdList);
			Cookie cookie = new Cookie(cookieName, jsonObject.toJSONString());
			response.addCookie(cookie);
		}

		return noticeIdList;
	}
}