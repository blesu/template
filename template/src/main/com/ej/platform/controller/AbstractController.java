/**
 * blesu
 * 2016. 3. 23. 오후 6:12:08
 */
package com.ej.platform.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author blesu
 *
 */
public abstract class AbstractController {
	@Autowired
	private HttpServletRequest request;

	/**
	 * 
	 * @return
	 */
	protected int getGameMemberId() {
		return (int) request.getAttribute("gid");
	}
}
