/**
 * AES123Utils.java
 * 2015. 11. 18. 오전 12:44:52
 */
package com.ej.platform.util;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * @author blesu
 *
 */
public class AES128Utils {

	/**
	 * 
	 * @param encKey
	 * @param msg
	 * @return
	 * @throws Exception
	 */
	public static String encrypt(String encKey, String msg) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		byte[] keyBytes = new byte[16];
		byte[] b = encKey.getBytes("UTF-8");

		int len = b.length;
		len = (len > keyBytes.length) ? keyBytes.length : b.length;
		System.arraycopy(b, 0, keyBytes, 0, len);

		SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
		IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);

		cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

		byte[] results = cipher.doFinal(msg.getBytes("UTF-8"));

		BASE64Encoder encoder = new BASE64Encoder();
		return encoder.encode(results);
	}

	/**
	 * 
	 * @param encKey
	 * @param encStr
	 * @return
	 * @throws Exception
	 */
	public static String decrypt(String encKey, String encStr) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

		byte[] keyBytes = new byte[16];
		byte[] b = encKey.getBytes("UTF-8");

		int len = b.length;
		len = (len > keyBytes.length) ? keyBytes.length : b.length;

		System.arraycopy(b, 0, keyBytes, 0, len);

		SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
		IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);

		cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
		BASE64Decoder decoder = new BASE64Decoder();

		byte[] results = cipher.doFinal(decoder.decodeBuffer(encStr));
		return new String(results, "UTF-8");
	}
}
